#include <queue>

using namespace std;

class MyStack {
 public:
  queue<int> data;
  MyStack() {}

  void push(int x) { data.emplace(x); }

  int pop() {
    if (data.empty()) {
      return NULL;
    }

    int ret = data.back();
    removeLastItem();

    return ret;
  }

  int top() {
    if (data.empty()) {
      return NULL;
    }

    return data.back();
  }

  bool empty() { return data.empty(); }

 private:
  void removeLastItem() {
    queue<int> nq;

    int dataLength = data.size();
    for (int i = 0; i < dataLength - 1; i++) {
      nq.emplace(data.front());
      data.pop();
    }

    data = nq;
  }
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack* obj = new MyStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * bool param_4 = obj->empty();
 */
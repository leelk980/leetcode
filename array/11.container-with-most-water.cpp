#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  int maxArea(vector<int>& height) {
    int maxArea = 0;
    for (int i = 0; i < height.size(); i++) {
      for (int j = height.size() - 1; j >= i + 1; j--) {
        int w = (j - i);
        int h = min(height.at(i), height.at(j));

        maxArea = max(maxArea, w * h);
        if (h == height.at(i)) {
          break;
        }
      }
    }

    return maxArea;
  }

  int bfMaxArea(vector<int>& height) {
    int maxArea = 0;
    for (int i = 0; i < height.size(); i++) {
      for (int j = i + 1; j < height.size(); j++) {
        int value = (j - i) * min(height.at(i), height.at(j));
        maxArea = max(maxArea, value);
      }
    }

    return maxArea;
  }
};

int main() {
  Solution solution = Solution();
  vector<int> height = {1, 8, 6, 2, 5, 4, 8, 3, 7};

  int result = solution.maxArea(height);

  cout << result << endl;
}
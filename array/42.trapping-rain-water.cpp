#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int trap(vector<int>& height) {
    int total = 0;

    unordered_map<int, int> leftMaxMap = {{0, height[0]}};
    for (int i = 1; i < height.size() - 1; i++) {
      int current = height.at(i);

      int prev = height.at(i - 1);
      int prevLeftMax = leftMaxMap[i - 1];
      if (prevLeftMax > prev) {
        leftMaxMap[i] = prevLeftMax;
      } else {
        leftMaxMap[i] = prev;
      }
    }

    int lastIndex = height.size() - 1;
    unordered_map<int, int> rightMaxMap = {{lastIndex, height[lastIndex]}};
    for (int i = lastIndex - 1; i > 0; i--) {
      int current = height.at(i);

      int prev = height.at(i + 1);
      int prevRightMax = rightMaxMap[i + 1];
      if (prevRightMax > prev) {
        rightMaxMap[i] = prevRightMax;
      } else {
        rightMaxMap[i] = prev;
      }
    }

    for (int i = 1; i < height.size() - 1; i++) {
      int current = height.at(i);
      int leftMax = leftMaxMap[i];
      int rightMax = rightMaxMap[i];

      if (leftMax > current && rightMax > current) {
        total += min(leftMax, rightMax) - current;
      }
    }

    return total;
  }
};

int main() {
  Solution solution = Solution();

  vector<int> height = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};

  int result = solution.trap(height);

  cout << result << endl;
}
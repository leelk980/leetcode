#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<int> twoSum(vector<int>& nums, int target) {
    unordered_map<int, int> hashMap;

    for (int i = 0; i < nums.size(); i++) {
      hashMap[nums.at(i)] = i;
    }

    for (int i = 0; i < nums.size(); i++) {
      int j = hashMap[target - nums.at(i)];
      if (j && i != j) {
        return {i, j};
      }
    }

    return {-1, -1};
  }
};

int main() {
  Solution solution = Solution();
  vector<int> nums = {1, 3, 4, 2};
  vector<int> result = solution.twoSum(nums, 6);

  cout << result.at(0) << ", " << result.at(1) << endl;
}
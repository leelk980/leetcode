#include <iostream>
#include <string>

using namespace std;

class Solution {
 public:
  bool backspaceCompare(string s, string t) { return strip(s) == strip(t); }

  string strip(string& str) {
    string ns;
    ns.reserve(str.size());
    for (char each : str) {
      if (each != '#') {
        ns.push_back(each);
      } else {
        if (!ns.empty()) ns.pop_back();
      }
    }

    return ns;
  }
};

int main() {
  Solution solution = Solution();

  bool result = solution.backspaceCompare("ab#c", "ad#c");

  cout << result << endl;
}
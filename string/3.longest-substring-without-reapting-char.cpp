#include <iostream>
#include <string>

using namespace std;

class Solution {
 public:
  int lengthOfLongestSubstring(string s) {
    size_t maxLength = 0;

    for (int i = 0; i < s.size() - maxLength; i++) {
      string subString;

      for (int j = i; j < s.size(); j++) {
        char current = s[j];
        if (subString.find(current) != -1) {
          break;
        }

        subString.push_back(current);
      }

      maxLength = max(maxLength, subString.size());
    }

    return maxLength;
  }
};

int main() {
  Solution solution = Solution();

  int result = solution.lengthOfLongestSubstring("abcabcbb");

  cout << result << endl;
}
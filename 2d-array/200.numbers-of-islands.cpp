#include <queue>
#include <vector>

using namespace std;

class Solution {
  queue<vector<int>> q;
  int m;
  int n;

 public:
  int numIslands(vector<vector<char>>& grid) {
    m = grid.size();
    n = grid[0].size();
    if (m == 0 || n == 0) {
      return 0;
    }

    int count = 0;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        char currentItem = grid[i][j];
        if (currentItem != '1') {
          continue;
        }

        count++;
        q.emplace(vector{i, j});

        while (!q.empty()) {
          vector<int> current = q.front();
          q.pop();
          if (current.empty()) break;

          checkNearBy(grid, current[0], current[1]);
        }
      }
    }

    return count;
  }

  void checkNearBy(vector<vector<char>>& grid, int i, int j) {
    grid[i][j] = '0';
    if (i > 0) {
      int upItem = grid[i - 1][j];
      if (upItem == '1') {
        q.emplace(vector{i - 1, j});
        grid[i - 1][j] = '0';
      }
    }

    if (j < n - 1) {
      int rightItem = grid[i][j + 1];
      if (rightItem == '1') {
        q.emplace(vector{i, j + 1});
        grid[i][j + 1] = '0';
      }
    }

    if (i < m - 1) {
      int downItem = grid[i + 1][j];
      if (downItem == '1') {
        q.emplace(vector{i + 1, j});
        grid[i + 1][j] = '0';
      }
    }

    if (j > 0) {
      int leftItem = grid[i][j - 1];
      if (leftItem == '1') {
        q.emplace(vector{i, j - 1});
        grid[i][j - 1] = '0';
      }
    }
  }
};
#include <stack>
#include <string>

using namespace std;

class Solution {
 public:
  bool isValid(string s) {
    stack<char> st;

    for (char each : s) {
      if (each == '(' || each == '{' || each == '[') {
        st.emplace(each);
        continue;
      }

      if (!(each == ')' || each == '}' || each == ']')) {
        continue;
      }

      if (st.empty()) {
        return false;
      }

      char top = st.top();
      if (each == ')' && top != '(') {
        return false;
      }

      if (each == '}' && top != '{') {
        return false;
      }

      if (each == ']' && top != '[') {
        return false;
      }

      st.pop();
    }

    if (st.empty()) {
      return true;
    } else {
      return false;
    }
  }
};
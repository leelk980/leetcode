#include <iostream>
#include <stack>
#include <string>
#include <vector>

using namespace std;

class Solution {
 public:
  string minRemoveToMakeValid(string s) {
    stack<int> st;
    string ns;
    for (int i = 0; i < s.size(); i++) {
      char current = s.at(i);

      if (current == ')') {
        if (st.empty()) continue;
        st.pop();
      }

      if (current == '(') {
        st.emplace(ns.size());
      }

      ns.push_back(current);
    }

    if (st.empty()) {
      return ns;
    }

    int stackLength = st.size();
    for (int i = 0; i < stackLength; i++) {
      int idx = st.top();
      st.pop();

      ns[idx] = NULL;
    }

    string nns;
    for (char each : ns) {
      if (!each) {
        continue;
      }

      nns.push_back(each);
    }

    return nns;
  }
};

int main() {
  Solution solution = Solution();

  string result = solution.minRemoveToMakeValid("))((");

  cout << result << endl;
}
#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode* left, TreeNode* right)
      : val(x), left(left), right(right) {}
};

class Solution {
  queue<TreeNode*> queue;

 public:
  vector<vector<int>> levelOrder(TreeNode* root) {
    unordered_map<TreeNode*, int> hashMap;
    vector<vector<int>> retVt;
    if (root == nullptr) {
      return retVt;
    }

    queue.emplace(root);
    hashMap[root] = 0;
    while (!queue.empty()) {
      TreeNode* current = queue.front();
      if (current == nullptr) break;
      queue.pop();

      int currentDepth = hashMap[current];
      if (current->left) {
        queue.emplace(current->left);
        hashMap[current->left] = currentDepth + 1;
      }

      if (current->right) {
        queue.emplace(current->right);
        hashMap[current->right] = currentDepth + 1;
      }

      if (currentDepth + 1 - retVt.size() == 1) {
        retVt.resize(currentDepth + 1);
      }

      retVt[currentDepth].emplace_back(current->val);
    }

    return retVt;
  }
};
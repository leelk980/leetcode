#include <queue>
#include <unordered_map>

using namespace std;
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode* left, TreeNode* right)
      : val(x), left(left), right(right) {}
};

class Solution {
  queue<TreeNode*> queue;

 public:
  int maxDepth(TreeNode* root) {
    if (root == nullptr) return 0;

    unordered_map<TreeNode*, int> hashMap;

    queue.emplace(root);
    hashMap[root] = 1;

    int maxDepth = 1;
    while (!queue.empty()) {
      TreeNode* current = queue.front();
      if (current == nullptr) break;
      queue.pop();
      int currentDepth = hashMap[current];

      if (current->left != nullptr) {
        queue.emplace(current->left);
        hashMap[current->left] = currentDepth + 1;
      }

      if (current->right != nullptr) {
        queue.emplace(current->right);
        hashMap[current->right] = currentDepth + 1;
      }

      maxDepth = max(maxDepth, currentDepth);
    }

    return maxDepth;
  }
};
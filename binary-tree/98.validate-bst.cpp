#include <algorithm>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;

struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right)
      : val(x), left(left), right(right) {}
};

/** 어려움 */
class Solution {
  queue<TreeNode *> q;
  unordered_map<TreeNode *, TreeNode *> hm;

 public:
  bool isValidBST(TreeNode *root) {
    q.emplace(root);

    while (!q.empty()) {
      TreeNode *current = q.front();
      if (!current) break;
      q.pop();

      if (current->left) {
        hm[current->left] = current;
        if (!compareAncestor(current->left)) {
          return false;
        }
        q.emplace(current->left);
      }

      if (current->right) {
        hm[current->right] = current;
        if (!compareAncestor(current->right)) {
          return false;
        }
        q.emplace(current->right);
      }
    }

    return true;
  }

  bool compareAncestor(TreeNode *node) {
    int targetValue = node->val;

    TreeNode *parent = hm[node];
    TreeNode *current = node;
    while (true) {
      bool isLeftSide = parent->left == current;

      if (isLeftSide && targetValue >= parent->val) {
        return false;
      } else if (!isLeftSide && targetValue <= parent->val) {
        return false;
      }

      if (!hm[parent]) {
        break;
      } else {
        current = parent;
        parent = hm[current];
      }
    }

    return true;
  }
};

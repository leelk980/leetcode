#include <iostream>
#include <queue>
#include <vector>

using namespace std;

struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right)
      : val(x), left(left), right(right) {}
};

class Solution {
  queue<TreeNode *> q;

 public:
  int countNodes(TreeNode *root) {
    if (!root) {
      return 0;
    }

    q.emplace(root);
    int count = 0;
    while (!q.empty()) {
      TreeNode *current = q.front();
      if (!current) break;
      q.pop();

      if (current->left) {
        q.emplace(current->left);
      }

      if (current->right) {
        q.emplace(current->right);
      }

      count += 1;
    }

    return count;
  }
};
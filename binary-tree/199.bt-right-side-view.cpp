#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode* left, TreeNode* right)
      : val(x), left(left), right(right) {}
};

class Solution {
  queue<TreeNode*> queue;

 public:
  vector<int> rightSideView(TreeNode* root) {
    if (root == nullptr) {
      return vector<int>{};
    }

    unordered_map<TreeNode*, int> hashMap;
    vector<vector<int>> levelVt;

    queue.emplace(root);
    hashMap[root] = 0;
    while (!queue.empty()) {
      TreeNode* current = queue.front();
      if (current == nullptr) break;
      queue.pop();

      int currentDepth = hashMap[current];
      if (current->left) {
        queue.emplace(current->left);
        hashMap[current->left] = currentDepth + 1;
      }

      if (current->right) {
        queue.emplace(current->right);
        hashMap[current->right] = currentDepth + 1;
      }

      if (currentDepth + 1 - levelVt.size() == 1) {
        levelVt.resize(currentDepth + 1);
      }

      levelVt[currentDepth].emplace_back(current->val);
    }

    vector<int> retVt;
    retVt.reserve(levelVt.size());
    for (int i = 0; i < levelVt.size(); i++) {
      retVt.emplace_back(levelVt[i].back());
    }

    return retVt;
  }
};
#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<int> bs(vector<int>& nums, int target, int il, int ir) {
    int left = il;
    int right = ir;
    int index = -1;
    while (left <= right) {
      int pivot = (left + right) / 2;

      if (nums.at(pivot) == target) {
        index = pivot;
        break;
      }
      if (nums.at(pivot) < target) {
        left = pivot + 1;
      }
      if (nums.at(pivot) > target) {
        right = pivot - 1;
      }
    }

    return vector<int>{left, right, index};
  }

  vector<int> searchRange(vector<int>& nums, int target) {
    vector<int> result = bs(nums, target, 0, nums.size() - 1);

    if (result.at(2) == -1) {
      return vector<int>{-1, -1};
    }

    int left = result.at(0);
    int first = result.at(2);
    while (left < first) {
      vector<int> resultL = bs(nums, target, left, first - 1);

      if (resultL.at(2) == -1) {
        break;
      }

      left = resultL.at(0);
      first = resultL.at(2);
    }

    int right = result.at(1);
    int last = result.at(2);
    while (last < right) {
      vector<int> resultR = bs(nums, target, last + 1, right);

      if (resultR.at(2) == -1) {
        break;
      }

      right = resultR.at(1);
      last = resultR.at(2);
    }

    return vector<int>{first, last};
  }
};

int main() {
  Solution solution = Solution();

  vector<int> nums = {5, 7, 7, 8, 8, 10};

  vector<int> result = solution.searchRange(nums, 8);

  cout << result.at(0) << " " << result.at(1) << endl;
}
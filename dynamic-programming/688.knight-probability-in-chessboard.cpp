#include <iostream>
#include <queue>
#include <vector>

using namespace std;

/** @todo 시간 초과 */
class Solution {
  vector<vector<double>> board;
  queue<vector<int>> q;

 public:
  double knightProbability(int n, int k, int row, int column) {
    board = vector(n, vector<double>(n));

    board[row][column] = 1;

    q.emplace(vector{row, column, 0});
    for (int i = 0; i < k; i++) {
      while (!q.empty()) {
        vector<int> current = q.front();
        if (current.empty()) break;
        if (current[2] != i) break;
        q.pop();

        proceedK(current[0], current[1], i);
      }
    }

    double ret = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        double item = board[i][j];
        ret += item;
      }
    }

    return ret;
  }

  void proceedK(int row, int column, int step) {
    move(row, column, -2, 1, step);
    move(row, column, -1, 2, step);
    move(row, column, 1, 2, step);
    move(row, column, 2, 1, step);
    move(row, column, 2, -1, step);
    move(row, column, 1, -2, step);
    move(row, column, -1, -2, step);
    move(row, column, -2, -1, step);
    board[row][column] = 0;
  }

  void move(int row, int column, int x, int y, int step) {
    double origin = board[row][column];
    int n = board.size();

    int newRow = row + x;
    int newCol = column + y;
    if (0 <= newRow && newRow < n && 0 <= newCol && newCol < n) {
      board[newRow][newCol] = board[newRow][newCol] + origin * (1.0 / 8.0);
      q.emplace(vector{newRow, newCol, step + 1});
    }
  }
};

int main() {
  Solution solution = Solution();
  double result = solution.knightProbability(3, 2, 0, 0);

  cout << result << endl;
}
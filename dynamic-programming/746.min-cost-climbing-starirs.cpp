#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int minCostClimbingStairs(vector<int>& cost) {
    // f(n) = min(f(n-1) + c(n-1), f(n-2) + c(n-2))
    unordered_map<int, int> dpMap = {{0, 0}, {1, 0}};

    int n = cost.size();

    for (int i = 2; i <= n; i++) {
      dpMap[i] = min(dpMap[i - 1] + cost[i - 1], dpMap[i - 2] + cost[i - 2]);
    }

    return dpMap[n];
  }
};
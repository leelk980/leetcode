#include <unordered_map>

using namespace std;

class Solution {
 public:
  int climbStairs(int n) {
    // fn = f(n-1) + f(n-2)
    unordered_map<int, int> dpMap = {{1, 1}, {2, 2}};

    for (int i = 3; i <= n; i++) {
      dpMap[i] = dpMap[i - 1] + dpMap[i - 2];
    }

    return dpMap[n];
  }
};
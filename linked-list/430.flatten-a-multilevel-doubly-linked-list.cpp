#include <iostream>
#include <stack>
#include <vector>

using namespace std;

class Node {
 public:
  int val;
  Node* prev;
  Node* next;
  Node* child;
};

class Solution {
 public:
  Node* flatten(Node* head) {
    stack<Node*> st;
    st.emplace(head);

    Node* prevNode = nullptr;
    while (!st.empty()) {
      Node* current = st.top();
      st.pop();

      if (!current) {
        continue;
      }
      if (current->next) {
        st.emplace(current->next);
      }
      if (current->child) {
        st.emplace(current->child);
      }
      if (prevNode) {
        prevNode->child = nullptr;
        prevNode->next = current;
        current->prev = prevNode;
      }

      prevNode = current;
    }

    return head;
  }
};

int main() {}
#include <unordered_map>
#include <vector>

using namespace std;

struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
 public:
  ListNode* detectCycle(ListNode* head) {
    unordered_map<int, vector<ListNode*>> hashMap;

    ListNode* current = head;
    while (current) {
      vector<ListNode*>& vt = hashMap[current->val];

      if (!vt.empty()) {
        for (auto each : vt) {
          if (each == current) return current;
        }
      }

      vt.emplace_back(current);

      current = current->next;
    }

    return NULL;
  }
};
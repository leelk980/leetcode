#include <array>
#include <iostream>
#include <unordered_map>

using namespace std;

struct ListNode {
  int val;
  ListNode* next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
 public:
  ListNode* reverseBetween(ListNode* head, int left, int right) {
    unordered_map<int, ListNode*> hashMap;

    ListNode* current = head;
    int idx = 1;
    while (current != nullptr) {
      hashMap[idx] = current;
      idx += 1;
      current = current->next;
    }

    for (int i = 1; i <= idx; i++) {
      if (i == left - 1) {
        hashMap[i]->next = hashMap[right];
        continue;
      }
      if (i == left) {
        hashMap[i]->next = hashMap[right]->next;
        continue;
      }
      if (left < i && i <= right) {
        hashMap[i]->next = hashMap[i - 1];
        continue;
      }
    }

    if (left == 1) {
      return hashMap[right];
    } else {
      return head;
    }
  }
};

int main() {
  Solution solution = Solution();

  array<ListNode*, 5> arr;

  for (int i = 0; i < 5; i++) {
    ListNode* node = new ListNode(i + 1);
    arr[i] = node;
  }

  for (int i = 0; i < 4; i++) {
    arr[i]->next = arr[i + 1];
  }

  ListNode* result = solution.reverseBetween(arr[0], 2, 4);

  ListNode* current = result;
  while (current != nullptr) {
    cout << current->val << "->";
    current = current->next;
  }

  cout << endl;

  // Clean up dynamically allocated memory
  for (int i = 0; i < 5; i++) {
    delete arr[i];
  }
}
